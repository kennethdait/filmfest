//
//  ViewController.swift
//  FilmFest
//
//  Created by kend on 10/10/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import UIKit

class LibraryViewController: UIViewController {

    @IBOutlet weak var libraryTableView: UITableView!
    @IBOutlet var dataService: MovieLibraryDataService!
    var movieManager = MovieManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataService.movieManager = movieManager
        libraryTableView.dataSource = dataService
        libraryTableView.delegate = dataService
        loadDummyData()
    }
    
    func loadDummyData()
    {
        dataService.movieManager?.addMovie(movie: Movie(title: "Action Movie", releaseDate: "1984"))
        dataService.movieManager?.addMovie(movie: Movie(title: "Horror Movie", releaseDate: "2011"))
        dataService.movieManager?.addMovie(movie: Movie(title: "Rom-Com Movie", releaseDate: "1975"))
        dataService.movieManager?.addMovie(movie: Movie(title: "Sci-Fi Movie", releaseDate: "2018"))
    }


}

