//
//  MovieCell.swift
//  FilmFest
//
//  Created by kend on 11/1/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import UIKit

class MovieCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configMovieCell(movieData: Movie)
    {
        self.textLabel?.text = movieData.title
        self.detailTextLabel?.text = movieData.releaseDate
    }

}
