//
//  MovieLibraryDataService.swift
//  FilmFest
//
//  Created by kend on 10/31/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import UIKit

enum LibrarySections: Int
{
    case MoviesToSee
    case MoviesSeen
}

class MovieLibraryDataService: NSObject {
    var movieManager: MovieManager?
}

extension MovieLibraryDataService: UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int { return 2 }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let librarySection = LibrarySections(rawValue: section) else { return 0 }
        guard let movieManager = movieManager else { fatalError() }
        
        switch librarySection
        {
        case .MoviesToSee:
            return movieManager.moviesToSeeCount
        case .MoviesSeen:
            return movieManager.moviesSeenCount
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let librarySection = LibrarySections(rawValue: indexPath.section) else { fatalError() }
        guard let movieManager = movieManager else { fatalError() }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCellID", for: indexPath) as! MovieCell
        
        let movieData = librarySection.rawValue == 0 ? movieManager.movieAtIndex(indexPath.row) : movieManager.checkedOffMovieAtIndex(indexPath.row)
        cell.configMovieCell(movieData: movieData)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let librarySection = LibrarySections(rawValue: section) else { fatalError() }
        
        let title = librarySection.rawValue == 0 ? "Movies To See" : "Movies Seen"
        
        return title
        
    }
    
    
}

extension MovieLibraryDataService: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let librarySection = LibrarySections(rawValue: indexPath.section) else { fatalError() }
        guard let movieManager = movieManager else { fatalError() }
        
        switch librarySection
        {
        case .MoviesToSee:
            movieManager.checkOffMovie(at: indexPath.row)
        case .MoviesSeen:
            movieManager.uncheckOffMovie(at: indexPath.row)
        }
        
        tableView.reloadData()
    }
}
