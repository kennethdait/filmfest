//
//  MovieLibraryDataServiceTests.swift
//  FilmFestTests
//
//  Created by kend on 11/1/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import XCTest
@testable import FilmFest

class MovieLibraryDataServiceTests: XCTestCase
{
    
    var sut: MovieLibraryDataService!
    var libraryTableView: UITableView!
    var tableViewMock: TableViewMock!

    let movie1 = Movie(title: "Action Movie")
    let movie2 = Movie(title: "Horror Movie")
    let movie3 = Movie(title: "Romantic-Comedy Movie")
    
    override func setUp()
    {
        sut = MovieLibraryDataService()
        sut.movieManager = MovieManager()
        let libraryVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LibraryViewControllerID") as! LibraryViewController)
        _ = libraryVC.view
        libraryTableView = libraryVC.libraryTableView
        libraryTableView.dataSource = sut
        libraryTableView.delegate = sut
        
        tableViewMock = TableViewMock.initMock(dataSource: sut)
    }

    override func tearDown()
    { super.tearDown() }

    // MARK: Initializations
    
    func testInit_DataService_NotNil()
    { XCTAssertNotNil(sut) }

    // MARK: Sections
    
    func testSections_SectionCount_ReturnsTwo()
    { XCTAssertEqual(libraryTableView.numberOfSections, 2) }
    
    func testSections_SectionOne_ReturnsMoviesToSeeCount()
    {
        sut.movieManager?.addMovie(movie: movie1)
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 0), 1)
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 1), 0)
    }
    
    func testSections_SectionTwo_ReturnsMoviesSeenCount()
    {
        sut.movieManager?.addMovie(movie: movie1)
        sut.movieManager?.addMovie(movie: movie2)
        sut.movieManager?.checkOffMovie(at: 0)
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 0), 1)
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 1), 1)
    }
    
    func testSections_SectionTitles_ReturnsCorrectStrings()
    {
        let section1Header = sut.tableView(libraryTableView, titleForHeaderInSection: 0)
        let section2Header = sut.tableView(libraryTableView, titleForHeaderInSection: 1)
        XCTAssertEqual(section1Header, "Movies To See")
        XCTAssertEqual(section2Header, "Movies Seen")
    }
    
    // MARK: Cells
    
    func testCells_ReturnsMovieCell()
    {
        sut.movieManager?.addMovie(movie: movie1)
        libraryTableView.reloadData()
        let cellQueried = libraryTableView.cellForRow(at: IndexPath(row: 0, section: 0))
        XCTAssertTrue(cellQueried is MovieCell)
    }
    
    func testCells_CellDequeuesProperly()
    {
        sut.movieManager?.addMovie(movie: movie1)
        tableViewMock.reloadData()
        _ = tableViewMock.dequeueReusableCell(withIdentifier: "movieCellID", for: IndexPath(row: 0, section: 0))
        XCTAssertTrue(tableViewMock.cellDequeuedProperly)
    }
    
    func testCells_SectionOne_ConfiguresCellData()
    {
        sut.movieManager?.addMovie(movie: movie3)
        tableViewMock.reloadData()
        let cellQueried = sut.tableView(tableViewMock, cellForRowAt: IndexPath(row: 0, section: 0)) as! MovieCellMock
        XCTAssertEqual(cellQueried.movieData, movie3)
    }
    
    func testCells_SectionTwo_ConfiguresCellData()
    {
        sut.movieManager?.addMovie(movie: movie2)
        sut.movieManager?.checkOffMovie(at: 0)
        tableViewMock.reloadData()
        let cellQueried = sut.tableView(tableViewMock, cellForRowAt: IndexPath(row: 0, section: 1)) as! MovieCellMock
        XCTAssertEqual(cellQueried.movieData, movie2)
    }
    
    // MARK: Cell Selections
    
    func testCellSelections_SectionOne_SelectionUpdatesTable()
    {
        sut.movieManager?.addMovie(movie: movie1)
        libraryTableView.delegate?.tableView!(libraryTableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(sut.movieManager?.moviesToSeeCount, 0)
        XCTAssertEqual(sut.movieManager?.moviesSeenCount, 1)
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 0), 0)
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 1), 1)
    }
    
    func testCellSelections_SectionTwo_SelectionUpdatesTable()
    {
        sut.movieManager?.addMovie(movie: movie1)
        sut.movieManager?.addMovie(movie: movie2)
        libraryTableView.delegate?.tableView!(libraryTableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        libraryTableView.delegate?.tableView!(libraryTableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(sut.movieManager?.moviesToSeeCount, 0)
        XCTAssertEqual(sut.movieManager?.moviesSeenCount, 2)
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 0), 0)
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 1), 2)
        
        libraryTableView.delegate?.tableView!(libraryTableView, didSelectRowAt: IndexPath(row: 0, section: 1))
        libraryTableView.delegate?.tableView!(libraryTableView, didSelectRowAt: IndexPath(row: 0, section: 1))
        
        XCTAssertEqual(sut.movieManager?.moviesToSeeCount, 2)
        XCTAssertEqual(sut.movieManager?.moviesSeenCount, 0)
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 0), 2)
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 1), 0)
    }
    
}
