//
//  Movie.swift
//  FilmFest
//
//  Created by kend on 10/31/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import Foundation

struct Movie
{
    var title: String
    var releaseDate: String?
    
    init(title: String, releaseDate: String? = nil)
    {
        self.title = title
        self.releaseDate = releaseDate
    }
}

extension Movie: Equatable {}

func==(lhs: Movie, rhs: Movie) -> Bool
{
    if lhs.title == rhs.title && lhs.releaseDate == rhs.releaseDate
    { return true }
    return false
}
