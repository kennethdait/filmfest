//
//  MovieManager.swift
//  FilmFest
//
//  Created by kend on 10/31/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import Foundation

class MovieManager
{
    var moviesToSee: [Movie] = []
    var moviesSeen: [Movie] = []
    var moviesToSeeCount: Int { return moviesToSee.count }
    var moviesSeenCount: Int { return moviesSeen.count }
    
    func addMovie(movie: Movie)
    {
        if !moviesToSee.contains(movie)
        { moviesToSee.append(movie) }
    }
    
    func movieAtIndex(_ index: Int) -> Movie
    { return moviesToSee[index] }
    
    func checkOffMovie(at index: Int)
    {
        let checkedMovie = moviesToSee.remove(at: index)
        moviesSeen.append(checkedMovie)
    }
    
    func checkedOffMovieAtIndex(_ index: Int) -> Movie
    { return moviesSeen[index] }
    
    func uncheckOffMovie(at index: Int)
    {
        let unCheckedMovie = moviesSeen.remove(at: index)
        moviesToSee.append(unCheckedMovie)
    }
    
    func clearArrays()
    {
        moviesToSee.removeAll()
        moviesSeen.removeAll()
    }
    
}
