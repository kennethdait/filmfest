//
//  MockExtensions.swift
//  FilmFestTests
//
//  Created by kend on 11/1/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import Foundation
@testable import FilmFest
import UIKit

extension MovieLibraryDataServiceTests
{
    class TableViewMock: UITableView
    {
        var cellDequeuedProperly = false
        
        class func initMock(dataSource: MovieLibraryDataService) -> TableViewMock
        {
            let mock = TableViewMock()
            mock.dataSource = dataSource
            mock.register(MovieCellMock.self, forCellReuseIdentifier: "movieCellID")
            return mock
        }
        
        override func dequeueReusableCell(withIdentifier identifier: String, for indexPath: IndexPath) -> UITableViewCell {
            cellDequeuedProperly = true
            return super.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        }
    }
    
    class MovieCellMock: MovieCell
    {
        var movieData: Movie!
        override func configMovieCell(movieData: Movie) {
            self.movieData = movieData
        }
    }
}

extension MovieCellTests
{
    class MockDataSource: NSObject, UITableViewDataSource
    {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
        { return 1 }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "movieCellID", for: indexPath) as! MovieCell
            return cell
        }
        
    }
}
