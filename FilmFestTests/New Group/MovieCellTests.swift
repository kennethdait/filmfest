//
//  MovieCellTests.swift
//  FilmFestTests
//
//  Created by kend on 11/1/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import XCTest
@testable import FilmFest

class MovieCellTests: XCTestCase
{

    var libraryVC: LibraryViewController!
    var tableView: UITableView!
    var mockDataSource: MockDataSource!
    
    override func setUp()
    {
        super.setUp()
        libraryVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LibraryViewControllerID") as! LibraryViewController)
        _ = libraryVC.view
        tableView = libraryVC.libraryTableView
        mockDataSource = MockDataSource()
        tableView.dataSource = mockDataSource
    }

    // MARK: Initializations
    
    func testInit_Config_SetsTextLabels()
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCellID", for: IndexPath(row: 0, section: 0)) as! MovieCell
        cell.configMovieCell(movieData: Movie(title: "Action Movie", releaseDate: "1984"))
        XCTAssertEqual(cell.textLabel?.text, "Action Movie")
        XCTAssertEqual(cell.detailTextLabel?.text, "1984")
    }

}
