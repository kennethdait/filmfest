//
//  MovieLibraryDataServiceTests.swift
//  FilmFestTests
//
//  Created by kend on 10/31/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import XCTest
@testable import FilmFest

class MovieLibraryDataServiceTests: XCTestCase
{
    
    var sut: MovieLibraryDataService!
    var libraryVC: LibraryViewController!
    var libraryTableView: UITableView!
    var tableViewMock: TableViewMock!
    
    let movie1 = Movie(title: "Action Movie")
    let movie2 = Movie(title: "Thriller Movie")
    let movie3 = Movie(title: "Romantic-Comedy Movie")
        
    override func setUp()
    {
        super.setUp()
        let sb = UIStoryboard(name: "Main", bundle: nil)
        libraryVC = (sb.instantiateViewController(withIdentifier: "LibraryViewControllerID") as! LibraryViewController)
        _ = libraryVC.view
        
        sut = MovieLibraryDataService()
        sut.movieManager = MovieManager()
        
        libraryTableView = libraryVC.libraryTableView
        libraryTableView.dataSource = sut
        libraryTableView.delegate = sut
        
        tableViewMock = TableViewMock.initMock(dataSource: sut)
        
    }
    
    override func tearDown() { super.tearDown() }
    
    // MARK: Initialization
    
    func testInit_DataService_NotNil()
    { XCTAssertNotNil(sut) }
    
    // MARK: Sections
    
    func testSections_SectionCount_ReturnsTwo()
    {
        let sections = libraryTableView.numberOfSections
        XCTAssertEqual(sections, 2)
    }
    
    func testSections_SectionOne_ReturnsMoviesToSeeCount()
    {
        sut.movieManager?.addMovie(movie: movie1)
        libraryTableView.reloadData()
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 0), 1)
        
        sut.movieManager?.addMovie(movie: movie2)
        libraryTableView.reloadData()
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 0), 2)
    }
    
    func testSections_SectionTwo_ReturnsMoviesSeenCount()
    {
        sut.movieManager?.addMovie(movie: movie3)
        sut.movieManager?.addMovie(movie: movie2)
        sut.movieManager?.checkOffMovie(at: 0)
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 1), 1)
        
        sut.movieManager?.checkOffMovie(at: 0)
        libraryTableView.reloadData()
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 1), 2)
    }
    
    // MARK: Cells
    
    func testCell_RowAtIndex_ReturnsMovieCell()
    {
        sut.movieManager?.addMovie(movie: movie1)
        libraryTableView.reloadData()
        let cellQueried = libraryTableView.dequeueReusableCell(withIdentifier: "movieCellID", for: IndexPath(row: 0, section: 0))
        XCTAssertTrue(cellQueried is MovieCell)
    }
    
    func testCells_CellDequeuedProperly()
    {
        
        sut.movieManager?.addMovie(movie: movie1)
        tableViewMock.reloadData()
        
        _ = tableViewMock.cellForRow(at: IndexPath(row: 0, section: 0))
        
        XCTAssertTrue(tableViewMock.cellDequeuedProperly)
    }
 
    func testCells_SectionOneConfig_ShouldSetCellData()
    {
        
        sut.movieManager?.addMovie(movie: movie1)
        tableViewMock.reloadData()
        
        let cell = tableViewMock.cellForRow(at: IndexPath(row: 0, section: 0)) as! MovieCellMock
        XCTAssertEqual(cell.movieData, movie1)
    }
    
    func testCells_SectionTwoConfig_ShouldSetCellData()
    {
        sut.movieManager?.addMovie(movie: movie2)
        sut.movieManager?.addMovie(movie: movie3)
        sut.movieManager?.checkOffMovie(at: 0)
        tableViewMock.reloadData()
        
        let cell = tableViewMock.cellForRow(at: IndexPath(row: 0, section: 1)) as! MovieCellMock
        XCTAssertEqual(cell.movieData, movie2)
    }
    
    func testTableViewSectionTitles_ShouldHaveCorrectStringValues()
    {
        let section1Title = libraryTableView.dataSource?.tableView!(libraryTableView, titleForHeaderInSection: 0)
        let section2Title = libraryTableView.dataSource?.tableView!(libraryTableView, titleForHeaderInSection: 1)
        XCTAssertEqual(section1Title, "Movies To See")
        XCTAssertEqual(section2Title, "Movies Seen")
    }
    
    // MARK: Selections
    
    func testCellSelection_SectionOne_ShouldCheckOffMovieToSee()
    {
        sut.movieManager?.addMovie(movie: movie1)
        sut.movieManager?.addMovie(movie: movie2)
        libraryTableView.delegate?.tableView!(libraryTableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(sut.movieManager?.moviesToSeeCount, 1)
        XCTAssertEqual(sut.movieManager?.moviesSeenCount, 1)
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 0), 1)
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 1), 1)
    }
    
    func testCellSelection_SectionTwo_ShouldCheckOffMovieSeen()
    {
        sut.movieManager?.addMovie(movie: movie1)
        sut.movieManager?.addMovie(movie: movie2)
        libraryTableView.delegate?.tableView!(libraryTableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        libraryTableView.delegate?.tableView!(libraryTableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(sut.movieManager?.moviesToSeeCount, 0)
        XCTAssertEqual(sut.movieManager?.moviesSeenCount, 2)
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 0), 0)
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 1), 2)
        
        libraryTableView.delegate?.tableView!(libraryTableView, didSelectRowAt: IndexPath(row: 0, section: 1))
        libraryTableView.delegate?.tableView!(libraryTableView, didSelectRowAt: IndexPath(row: 0, section: 1))
        
        XCTAssertEqual(sut.movieManager?.moviesToSeeCount, 2)
        XCTAssertEqual(sut.movieManager?.moviesSeenCount, 0)
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 0), 2)
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 1), 0)
        
    }
}
