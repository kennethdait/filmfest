//
//  LibraryViewControllerTests.swift
//  FilmFestTests
//
//  Created by kend on 10/31/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import XCTest
@testable import FilmFest

class LibraryViewControllerTests: XCTestCase {

    var sut: LibraryViewController!
    
    override func setUp()
    {
        super.setUp()
        sut = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LibraryViewControllerID") as! LibraryViewController)
        _ = sut.view
    }

    override func tearDown() { super.tearDown() }

    // MARK: Initializations
    
    func testInit_LibraryVC_NotNil()
    { XCTAssertNotNil(sut) }

    // MARK: Data Source
    
    func testInit_ViewDidLoad_SetsDataSource()
    {
        XCTAssertNotNil(sut.libraryTableView.dataSource)
        XCTAssertTrue(sut.libraryTableView.dataSource is MovieLibraryDataService)
    }
    
    // MARK: Delegate
    func testInit_ViewDidLoad_SetsDelegate()
    {
        XCTAssertNotNil(sut.libraryTableView.delegate)
        XCTAssertTrue(sut.libraryTableView.delegate is MovieLibraryDataService)
    }
    
}
