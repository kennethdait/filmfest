//
//  MovieManagerTests.swift
//  FilmFestTests
//
//  Created by kend on 10/31/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import XCTest
@testable import FilmFest

class MovieManagerTests: XCTestCase {

    var sut: MovieManager!
    
    let movie1 = Movie(title: "Action Movie")
    let movie2 = Movie(title: "Horror Movie")
    let movie3 = Movie(title: "Romantic-Comedy Movie")
    
    override func setUp()
    {
        super.setUp()
        sut = MovieManager()
    }

    override func tearDown()
    { super.tearDown() }

    // MARK: Initializations
    
    func testInit_MovieManager_NotNil()
    { XCTAssertNotNil(sut) }

    func testInit_InitialCounts_ReturnZero()
    {
        XCTAssertEqual(sut.moviesToSeeCount, 0)
        XCTAssertEqual(sut.moviesSeenCount, 0)
    }
    
    // MARK: Add & Query Movies
    
    func testAdd_IncrementsMoviesToSeeCount()
    {
        sut.addMovie(movie: movie1)
        XCTAssertEqual(sut.moviesToSeeCount, 1)
        sut.addMovie(movie: movie2)
        XCTAssertEqual(sut.moviesToSeeCount, 2)
    }
    
    func testQuery_ReturnsMovieAtIndex()
    {
        sut.addMovie(movie: movie1)
        let movieQueried = sut.movieAtIndex(0)
        XCTAssertEqual(movieQueried, movie1)
    }
    
    // MARK: Checkoff Movies
    
    func testCheckoff_UpdatesCountsRespectively()
    {
        sut.addMovie(movie: movie1)
        sut.addMovie(movie: movie2)
        sut.checkOffMovie(at: 0)
        XCTAssertEqual(sut.moviesToSeeCount, 1)
        XCTAssertEqual(sut.moviesSeenCount, 1)
    }
    
    func testCheckoff_ReturnsCheckedOffMovieAtIndex()
    {
        sut.addMovie(movie: movie3)
        sut.checkOffMovie(at: 0)
        let movieQueried = sut.checkedOffMovieAtIndex(0)
        XCTAssertEqual(movieQueried, movie3)
    }
    
    // MARK: Un-Checkoff Movies
    
    func testUnCheckoff_UpdatesCountsRespectively()
    {
        sut.addMovie(movie: movie1)
        sut.addMovie(movie: movie2)
        sut.checkOffMovie(at: 0)
        sut.checkOffMovie(at: 0)
        XCTAssertEqual(sut.moviesToSeeCount, 0)
        XCTAssertEqual(sut.moviesSeenCount, 2)
        sut.uncheckOffMovie(at: 0)
        XCTAssertEqual(sut.moviesToSeeCount, 1)
        XCTAssertEqual(sut.moviesSeenCount, 1)
        sut.uncheckOffMovie(at: 0)
        XCTAssertEqual(sut.moviesToSeeCount, 2)
        XCTAssertEqual(sut.moviesSeenCount, 0)
    }
    
    // MARK: Verifications
    
    func testVerifications_Duplicates_NotAddedToMoviesToSee()
    {
        let movie1 = Movie(title: "Action Movie")
        let movie2 = Movie(title: "Action Movie")
        sut.addMovie(movie: movie1)
        sut.addMovie(movie: movie2)
        XCTAssertEqual(sut.moviesToSeeCount, 1)
    }
    
    // MARK: Clear Data
    
    func testClearArrays_ClearsBothArrays()
    {
        sut.addMovie(movie: movie1)
        sut.addMovie(movie: movie2)
        sut.addMovie(movie: movie3)
        sut.checkOffMovie(at: 0)
        sut.checkOffMovie(at: 0)
        XCTAssertEqual(sut.moviesToSeeCount, 1)
        XCTAssertEqual(sut.moviesSeenCount, 2)
        sut.clearArrays()
        XCTAssertEqual(sut.moviesToSeeCount, 0)
        XCTAssertEqual(sut.moviesSeenCount, 0)
    }
    
}

