//
//  MovieStructTests.swift
//  FilmFestTests
//
//  Created by kend on 10/31/18.
//  Copyright © 2018 kenanigans.com. All rights reserved.
//

import XCTest
@testable import FilmFest

class MovieStructTests: XCTestCase
{

    override func setUp() { super.setUp() }
    override func tearDown() { super.tearDown() }

    // MARK: Initializations
    
    func testInit_MovieWithTitle()
    {
        let movie = Movie(title: "Action Movie")
        XCTAssertNotNil(movie)
        XCTAssertEqual(movie.title, "Action Movie")
    }
    
    func testInit_MovieWithOptionalReleaseDate()
    {
        let movieWithoutDate = Movie(title: "Action Movie")
        let movieWithDate = Movie(title: "Adventure Movie", releaseDate: "1984")
        XCTAssertNil(movieWithoutDate.releaseDate)
        XCTAssertNotNil(movieWithDate.releaseDate)
        XCTAssertEqual(movieWithDate.releaseDate, "1984")
    }
    
    // MARK: Equatable

    func testEquatable_SameTitle_NilReleaseDate_ReturnsEqual()
    {
        let movie1 = Movie(title: "Action Movie")
        let movie2 = Movie(title: "Action Movie")
        XCTAssertNil(movie1.releaseDate)
        XCTAssertNil(movie2.releaseDate)
        XCTAssertEqual(movie1, movie2)
    }
    
    func testEquatable_SameTitle_SameReleaseDate_ReturnsEqual()
    {
        let movie1 = Movie(title: "Action Movie", releaseDate: "1984")
        let movie2 = Movie(title: "Action Movie", releaseDate: "1984")
        XCTAssertEqual(movie1, movie2)
    }
    
    func testEquatable_DifferentTitle_SameReleaseDate_ReturnsNotEqual()
    {
        let movie1 = Movie(title: "Action Movie", releaseDate: "1984")
        let movie2 = Movie(title: "Horror Movie", releaseDate: "1984")
        XCTAssertNotEqual(movie1, movie2)
    }
    
    func testEquatable_SameTitle_DifferentReleaseDate_ReturnsNotEqual()
    {
        let movie1 = Movie(title: "Horror Movie", releaseDate: "1984")
        let movie2 = Movie(title: "Horror Movie", releaseDate: "2011")
        XCTAssertNotEqual(movie1, movie2)
    }
    
}
